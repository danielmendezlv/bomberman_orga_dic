package ventana;

import java.util.HashMap;
import java.util.Map;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import constantes.Accion;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import pojo.Usuario;
import java.awt.Toolkit;

public class Datos extends javax.swing.JFrame {

    HashMap<String, Usuario> listaUsuarios;
    HashMap<String, String> listaPendientes;
    String usuarioSeleccionado = "";
    Accion accion;
    JTable jt;
    
    public Datos() {
        initComponents();
    }

    public Datos(HashMap<String, Usuario> listaUsuarios, HashMap<String, String> listaPendientes, Accion accion) throws IOException{
        initComponents();
        setLocationRelativeTo(null);
        this.listaUsuarios = listaUsuarios;
        this.listaPendientes = listaPendientes;
        this.accion = accion;
        this.btnAccion2.setVisible(false);
        
        if(accion == Accion.VERUSUARIOS){
            setTitle("Ver Usuarios");
            btnAccion.setVisible(false);
        }else if(accion == Accion.ELIMINAR){
            setTitle("Eliminar Usuarios");
            btnAccion.setText("Eliminar");
        }else if(accion == Accion.CONFIRMAR){
            setTitle("Confirmar Usuarios");
            btnAccion.setText("Confirmar");
            btnAccion2.setVisible(true);
        }else if(accion == Accion.VERTIEMPOS){
            setTitle("TOP Tiempos");
            btnAccion.setVisible(false);
        }else if(accion == Accion.VERPUNTOS){
            setTitle("Top Puntos");
            btnAccion.setVisible(false);
        }
        llenarTabla();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        btnAccion = new javax.swing.JButton();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        btnAccion1 = new javax.swing.JButton();
        btnAccion2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Datos");
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/bomberman.png")));
        setUndecorated(true);

        btnAccion.setText("Accion");
        btnAccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAccionActionPerformed(evt);
            }
        });

        jLayeredPane2.setLayout(new java.awt.BorderLayout());

        btnAccion1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/izq_32.png"))); // NOI18N
        btnAccion1.setText("Volver");
        btnAccion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAccion1ActionPerformed(evt);
            }
        });

        btnAccion2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancel_32.png"))); // NOI18N
        btnAccion2.setText("Declinar");
        btnAccion2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAccion2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLayeredPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 439, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnAccion2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAccion1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLayeredPane2)
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addComponent(btnAccion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAccion2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                        .addComponent(btnAccion1)))
                .addContainerGap())
        );
        jLayeredPane1.setLayer(btnAccion, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jLayeredPane2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(btnAccion1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(btnAccion2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        getContentPane().add(jLayeredPane1, java.awt.BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAccionActionPerformed
        if(usuarioSeleccionado.length() > 0){
            if(accion == Accion.ELIMINAR){
                listaUsuarios.remove(usuarioSeleccionado);
                Datos vd = null;
                try {
                    vd = new Datos(listaUsuarios, listaPendientes, Accion.ELIMINAR);
                } catch (IOException ex) {
                    Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
                }
                vd.setVisible(true);
                this.dispose();
            }else if(accion == Accion.CONFIRMAR){
                listaUsuarios.put(usuarioSeleccionado, new Usuario(usuarioSeleccionado, listaPendientes.get(usuarioSeleccionado)));
                listaPendientes.remove(usuarioSeleccionado);
                Datos vd = null;
                try {
                    vd = new Datos(listaUsuarios, listaPendientes, Accion.CONFIRMAR);
                } catch (IOException ex) {
                    Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
                }
                vd.setVisible(true);
                this.dispose();
            }
        }
    }//GEN-LAST:event_btnAccionActionPerformed

    private void btnAccion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAccion1ActionPerformed
        InicioAdmin via = new InicioAdmin(listaUsuarios, listaPendientes);
        via.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnAccion1ActionPerformed

    private void btnAccion2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAccion2ActionPerformed
        if(usuarioSeleccionado.length() > 0){
            listaPendientes.remove(usuarioSeleccionado);
            Datos vd = null;
            try {
                vd = new Datos(listaUsuarios, listaPendientes, Accion.CONFIRMAR);
            } catch (IOException ex) {
                Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            }
            vd.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_btnAccion2ActionPerformed

    private void llenarTabla() throws IOException{
        if(accion == accion.VERUSUARIOS){
            setTitle("Ver Usuarios");
            btnAccion.setVisible(false);
            
            HashMap<String, Usuario> datos = this.listaUsuarios;
            String data[][] = new String[datos.size()][3];
            int i=0;
            for (Map.Entry me : datos.entrySet()) {
                data[i][0] = i+"";
                data[i][1] = me.getKey().toString();
                Usuario uo = (Usuario) me.getValue();
                data[i][2] = uo.getPassword();
                
                i++;
            }
            String column[]={"No.","Usuario","Password"};
            jt=new JTable(data,column);    
            jt.setBounds(30,40,200,900);
            
            JScrollPane sp=new JScrollPane(jt);
            jLayeredPane2.add(sp);
        }else if(accion == Accion.ELIMINAR){
            HashMap<String, Usuario> datos = this.listaUsuarios;
            String data[][] = new String[datos.size()][3];
            int i=0;
            for (Map.Entry me : datos.entrySet()) {
                data[i][0] = i+"";
                data[i][1] = me.getKey().toString();
                Usuario uo = (Usuario) me.getValue();
                data[i][2] = uo.getPassword();
                
                i++;
            }
            String column[]={"No.","Usuario","Password"};
            jt=new JTable(data,column);    
            jt.setBounds(30,40,200,900);
            
            JScrollPane sp=new JScrollPane(jt);
            
            jt.setCellSelectionEnabled(true);  
            ListSelectionModel select= jt.getSelectionModel();  
            select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            select.addListSelectionListener(new ListSelectionListener() {  
                public void valueChanged(ListSelectionEvent e) {  
                    int[] row = jt.getSelectedRows();
                    usuarioSeleccionado = jt.getValueAt(row[0], 1).toString();
                }
            });
            jLayeredPane2.add(sp);
        }else if(accion == Accion.CONFIRMAR){
            HashMap<String, String> datos = this.listaPendientes;
            String data[][] = new String[datos.size()][3];
            int i=0;
            for (Map.Entry me : datos.entrySet()) {
                data[i][0] = i+"";
                data[i][1] = me.getKey().toString();
                data[i][2] = me.getValue().toString();
                
                i++;
            }
            String column[]={"No.","Usuario","Password"};
            jt=new JTable(data,column);    
            jt.setBounds(30,40,200,900);
            
            JScrollPane sp=new JScrollPane(jt);
            
            jt.setCellSelectionEnabled(true);  
            ListSelectionModel select= jt.getSelectionModel();  
            select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            select.addListSelectionListener(new ListSelectionListener() {  
                public void valueChanged(ListSelectionEvent e) {
                    int[] row = jt.getSelectedRows();
                    usuarioSeleccionado = jt.getValueAt(row[0], 1).toString();
                }
            });
            jLayeredPane2.add(sp);
        }else if(accion == Accion.VERTIEMPOS){
            String contenido = "TOP 10 Tiempos\n";
            Map<Integer, Usuario> listaDatos = new HashMap<>(); 
            
            //De todos los usuarios
            for (Map.Entry me : listaUsuarios.entrySet()) {
                Usuario user = (Usuario) me.getValue();
                //de cada usuario su lista de partidas
                for(int k=0; k<user.getListaPartidas().size(); k++){
                    //de cada partida se guarda: (tiempo, (usuario:nombre, nivel, puntos))
                    Usuario part = new Usuario(user.getUsuario(), "");
                    part.nivel = user.getListaPartidas().get(k).getNivelMax();
                    part.numero = user.getListaPartidas().get(k).getTiempo();
                    
                    listaDatos.put(part.numero, part);
                }
            }
            
            Map<Integer, Usuario> treeMap = new TreeMap<>(listaDatos);
            Set set = treeMap.entrySet();
            Iterator it = set.iterator();
            int i=0;
            String data[][] = new String[treeMap.size()][3];
            while(it.hasNext()) {
                if(i<10){
                    Map.Entry me = (Map.Entry)it.next();
                    Usuario user = (Usuario) me.getValue();
                    data[i][0] = user.getUsuario();
                    data[i][1] = user.nivel+"";
                    data[i][2] = user.numero+"";
                    contenido+=user.getUsuario()+" "+user.nivel+""+user.numero+"\n";
                }
                i++;
            }
            String column[]={"Nombre","Nivel","Tiempo"};
            jt=new JTable(data,column);    
            jt.setBounds(30,40,200,900);
            
            JScrollPane sp=new JScrollPane(jt);
            jLayeredPane2.add(sp);
            escribirArchivo("Tiempos", contenido);
        }else if(accion == Accion.VERPUNTOS){
            String contenido = "TOP 10 Puntos\n";
            Map<Integer, Usuario> listaDatos = new HashMap<>(); 
            
            //De todos los usuarios
            for (Map.Entry me : listaUsuarios.entrySet()) {
                Usuario user = (Usuario) me.getValue();
                //de cada usuario su lista de partidas
                for(int k=0; k<user.getListaPartidas().size(); k++){
                    //de cada partida se guarda: (tiempo, (usuario:nombre, nivel, puntos))
                    Usuario part = new Usuario(user.getUsuario(), "");
                    part.nivel = user.getListaPartidas().get(k).getNivelMax();
                    part.numero = user.getListaPartidas().get(k).getPuntos();
                    
                    listaDatos.put(part.numero, part);
                }
            }
            
            Map<Integer, Usuario> treeMap = new TreeMap<>(listaDatos);
            Set set = treeMap.entrySet();
            Iterator it = set.iterator();
            int i=0;
            String data[][] = new String[treeMap.size()][3];
            while(it.hasNext()) {
                if(i<10){
                    Map.Entry me = (Map.Entry)it.next();
                    Usuario user = (Usuario) me.getValue();
                    data[i][0] = user.getUsuario();
                    data[i][1] = user.nivel+"";
                    data[i][2] = user.numero+"";
                    contenido+=user.getUsuario()+" "+user.nivel+""+user.numero+"\n";
                }
                i++;
            }
            String column[]={"Nombre","Nivel","Puntos"};
            jt=new JTable(data,column);    
            jt.setBounds(30,40,200,900);
            
            JScrollPane sp=new JScrollPane(jt);
            jLayeredPane2.add(sp);
            escribirArchivo("Puntos", contenido);
        }
        jLayeredPane2.setSize(300,200);
        jLayeredPane2.setVisible(true);
    }
    
    private void escribirArchivo(String nombre, String contenido) throws IOException{
        String path = System.getProperty("user.home") + "\\Desktop\\"+nombre+".rep";
        Files.write( Paths.get(path), contenido.getBytes());
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Datos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Datos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Datos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Datos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Datos(null, null, null).setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAccion;
    private javax.swing.JButton btnAccion1;
    private javax.swing.JButton btnAccion2;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JLayeredPane jLayeredPane2;
    // End of variables declaration//GEN-END:variables
}
