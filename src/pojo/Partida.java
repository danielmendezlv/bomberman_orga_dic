package pojo;

public class Partida {
    private int tiempo;
    private int puntos;
    private int nivelMax;

    public Partida(int tiempo, int puntos, int nivelMax) {
        this.tiempo = tiempo;
        this.puntos = puntos;
        this.nivelMax = nivelMax;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getNivelMax() {
        return nivelMax;
    }

    public void setNivelMax(int nivelMax) {
        this.nivelMax = nivelMax;
    }
    
}
