package pojo;

import java.util.ArrayList;

public class Usuario {
    private String usuario;
    private String password;
    private ArrayList<Partida> listaPartidas;
    public int numero;
    public int nivel;
    
    public Usuario(String usuario, String password) {
        this.usuario = usuario;
        this.password = password;
        this.listaPartidas = new ArrayList<>();
        this.numero = 0;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<Partida> getListaPartidas() {
        return listaPartidas;
    }

    public void setListaPartidas(ArrayList<Partida> listaPartidas) {
        this.listaPartidas = listaPartidas;
    }
    
}
