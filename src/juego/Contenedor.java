package juego;

import entities.Enemigo;
import entities.Jugador;
import entities.Mapa;
import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.JPanel;
import pojo.Usuario;

public class Contenedor extends JFrame{
    
    public Contenedor(Jugador jugador, Mapa mapa, HashMap<String, Usuario> listaUsuarios, HashMap<String, String> listaPendientes, String nombreUsuario) {
        super("Bomberman");
        JPanel generarJuego = new GeneracionJuego(jugador, mapa, this, listaUsuarios, listaPendientes, nombreUsuario);
        add(generarJuego);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }
    
}
