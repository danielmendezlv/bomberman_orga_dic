package juego;


import jnpout32.pPort;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jefferson
 */
public class EnviarMatriz {
    
    char[][] arregloTablero;
    boolean puedeTrabajar;

    public EnviarMatriz() {
        puedeTrabajar = true;
    }

    
    
    public char[][] getArregloTablero() {
        return arregloTablero;
    }

    public void setArregloTablero(char[][] arregloTablero) {
        this.arregloTablero = arregloTablero;
    }

    public boolean isPuedeTrabajar() {
        return puedeTrabajar;
    }

    public void setPuedeTrabajar(boolean puedeTrabajar) {
        this.puedeTrabajar = puedeTrabajar;
    }
    
    
    
    public void Pintar(){
        puedeTrabajar = false;
        
        pPort puerto = new pPort();
        short direccion = 0x378;
        short valorClock; // 0
        short valorData;
        
      
        for(int x=0; x<12; x++){
            for(int y=11;  y>=0; y--){

                valorClock  =  0x0;
                puerto.output(valorClock); 

                if(arregloTablero[y][x] != ' '){
                    valorClock  =  0x3;
                    puerto.output(valorClock);                    
                }else{
                    valorClock  =  0x2;
                    puerto.output(valorClock);
                } 
            }
            try{
                Thread.sleep(2);
            }catch(Exception e){

            } 
        }
        puedeTrabajar=true;       
    }        
}
