package juego;

import entities.Bomba;
import entities.Enemigo;
import entities.Jugador;
import entities.Mapa;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import pojo.Partida;
import pojo.Usuario;
import ventana.InicioUsuario;

public class GeneracionJuego extends JPanel implements KeyListener{
    Jugador jugador;
    Mapa mapa;
    Enemigo ene;
    boolean gana, pierde;
    private BufferedImage bfiGrama, bfiBomba, bfiFuego, bfiJugador, bfiLadrillo, bfiVidas, bfiPausa, bfiPausaInstrucciones, bfiEnemigo, bfiBonus, bfiLlave;
    private String strGrama="/assets/grass.png", strBomba="/assets/bomb.png";
    private String strFuego="/assets/exposion.png", strJugador="/assets/bomber.png";
    private String strLadrillo = "/assets/brick.png", strVidas="/assets/bomber_s.png";
    private String strPausa = "/assets/pausa.png", strEnemigo="/assets/enemigo.png"; 
    private String strPausaInstrucciones = "/assets/p.png", strBonus = "/assets/bonus.png", strLlave = "/assets/key.png"; 
    private ArrayList<Bomba> lstBombas;
    private int h, m, s, cs;
    boolean pausa;
    EnviarMatriz matrizFisico;
    boolean testSoloJuego = false;
    JFrame contenedor;
    HashMap<String, Usuario> listaUsuarios;
    HashMap<String, String> listaPendientes;
    String nombreUsuario;
    
    @Override
    public void keyTyped(KeyEvent e) {
        
    }
    
    @Override
    public boolean isFocusTraversable() {
        return true;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
        if(!pausa){
            switch (e.getKeyCode()) {               
                case KeyEvent.VK_DOWN:
                    jugador.abajo();
                    break;
                case KeyEvent.VK_UP: 
                    jugador.arriba();
                    break;
                case KeyEvent.VK_RIGHT: 
                    jugador.derecha();
                    break;
                case KeyEvent.VK_LEFT: 
                    jugador.izquierda();
                    break;
                case KeyEvent.VK_X: 
                    if(jugador.getBombasSueltas() < jugador.getBombasDisponibles()){
                        crearBomba();
                    } 
                    break;
            }
        }
        
        switch (e.getKeyCode()) { 
            case KeyEvent.VK_P:
                if(!pausa){
                    if(timerMovimiento.isRunning()){
                        timerMovimiento.stop();
                    }
                    pausa = true;
                    repaint();
                    for(int i=0; i< lstBombas.size(); i++){
                        lstBombas.get(i).getTimer().stop();
                    }
                }else{
                    timerMovimiento.start();                    
                    pausa=false;
                    for(int i=0; i< lstBombas.size(); i++){
                        lstBombas.get(i).getTimer().start();
                    }
                }
                break; 
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }

    public GeneracionJuego(Jugador jugador, Mapa mapa, JFrame contenedor, HashMap<String, Usuario> listaUsuarios, HashMap<String, String> listaPendientes, String nombreUsuario) {
        super();
        this.contenedor = contenedor;
        this.nombreUsuario = nombreUsuario;
        this.listaUsuarios = listaUsuarios;
        this.listaPendientes = listaPendientes;
        addKeyListener(this);
        matrizFisico= new EnviarMatriz();
        lstBombas =  new ArrayList<Bomba>();
        this.jugador = jugador;
        this.mapa= mapa;  
        this.pierde=false;
        this.gana=false;
        this.pausa=false;
        try{
            bfiBomba =  ImageIO.read(getClass().getResource(strBomba));
            bfiGrama =   ImageIO.read(getClass().getResource(strGrama));
            bfiFuego =  ImageIO.read(getClass().getResource(strFuego));
            bfiJugador =   ImageIO.read(getClass().getResource(strJugador));
            bfiLadrillo =   ImageIO.read(getClass().getResource(strLadrillo)); 
            bfiVidas =   ImageIO.read(getClass().getResource(strVidas)); 
            bfiPausa = ImageIO.read(getClass().getResource(strPausa)); 
            bfiPausaInstrucciones = ImageIO.read(getClass().getResource(strPausaInstrucciones)); 
            bfiEnemigo = ImageIO.read(getClass().getResource(strEnemigo)); 
            bfiBonus = ImageIO.read(getClass().getResource(strBonus)); 
            bfiLlave = ImageIO.read(getClass().getResource(strLlave));
        }catch(Exception e){
            System.out.println("Error al cargar las imagenes");
        }
        Dimension dimension = new Dimension(bfiBomba.getWidth()*12, bfiBomba.getHeight()*12+50);
        setPreferredSize(dimension);
        timerMovimiento.start();
        ene = new Enemigo(8,2, jugador.getMapa());
        if(!testSoloJuego){
            timerPintarMatriz.start();
        }        
    }
    
    public void finPartida(int tiempo, int puntos){
        Usuario u = listaUsuarios.get(nombreUsuario);
        u.getListaPartidas().add(new Partida(tiempo, puntos, 1));
        
        InicioUsuario viu = new InicioUsuario(listaUsuarios, listaPendientes, nombreUsuario);
        viu.setVisible(true);
        
        contenedor.dispose();
    }
    
    @Override
    public void paintComponent(Graphics g) {       
        
        Graphics2D g2d = (Graphics2D) g;
        System.out.println("------------------Carga------------------");
        //Recorro el la matriz del mapa
        for(int j=0;j<12;j++) {
            for(int i=0;i<12;i++) {
                //Jugador
                if(jugador.getX()==i&&jugador.getY()==j){
                    g2d.drawImage(bfiJugador, i*50, j*50, this);
                    System.out.print("J");
                }else//Enemigo
                if(ene.getX()==i&&ene.getY()==j){
                    g2d.drawImage(bfiEnemigo, i*50, j*50, this);
                    System.out.print("E");
                }else//Bonus con ladrillo 
                if(mapa.arregloTablero[j][i]=='B'){
                    g2d.drawImage(bfiLadrillo, i*50, j*50, this);
                    System.out.print("B");
                }
                else//Bonus Sin Ladrillo
                if(mapa.arregloTablero[j][i]=='b'){
                    g2d.drawImage(bfiBonus, i*50, j*50, this);
                    System.out.print("b");
                }
                else//Llave con ladrillo 
                if(mapa.arregloTablero[j][i]=='L'){
                    g2d.drawImage(bfiLadrillo, i*50, j*50, this);
                    System.out.print("L");
                }
                else//Llave sin ladrillo 
                if(mapa.arregloTablero[j][i]=='l'){
                    g2d.drawImage(bfiLlave, i*50, j*50, this);
                    System.out.print("l");
                }
                else //'Ladrillos'
                if(mapa.arregloTablero[j][i]=='X'){
                    g2d.drawImage(bfiLadrillo, i*50, j*50, this);
                    System.out.print("X");
                }else 
                //Nada
                if(mapa.arregloTablero[j][i]==' '){
                    g2d.drawImage(bfiGrama, i*50, j*50, this);
                    System.out.print(" ");
                }else
                if(mapa.arregloTablero[j][i]=='V'){
                    g2d.drawImage(bfiBomba, i*50, j*50, this);
                    System.out.print("V");
                }else
                if(mapa.arregloTablero[j][i]=='K'){
                   g2d.drawImage(bfiFuego, i*50, j*50, this);
                   System.out.print("K");
                }
                
            }
            System.out.println("");
        }
        //Vidas
        for(int i=0; i <jugador.getVidas(); i++){
            g2d.drawImage(bfiVidas, (9+i)*50,(12*50),this);
        }
        //Instrucciones pausa       
        g2d.drawImage(bfiPausaInstrucciones, (3)*50,(12*50),this);       
        //Cronometro
        

        g.setFont(new Font("TimesRoman", Font.PLAIN, 30)); 
        String tiempo = (m<=9?"0":"")+m+":"+(s<=9?"0":"")+s+":"+(cs<=9?"0":"")+cs;
        System.out.println("--------------------tiempo ---------------");
        System.out.println(tiempo);
       
        g2d.clearRect(5,13*50-50,130,100);
        g2d.drawString(tiempo, 5, 13*50-15);
        
        if(pausa){
            g2d.drawImage(bfiPausa,3*50,4*50,this);
        }
    }
    
  
      
    ActionListener actionListenerJugador = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {  
            /* Reloj */
            ++cs; 
            if(cs==100){
                cs = 0;
                ++s;
            }
            if(s==60) 
            {
                s = 0;
                ++m;
            }
            if(m==60)
            {
                m = 0;
                ++h;
            }
            repaint();
        }
    };
    Timer timerMovimiento = new Timer(10, actionListenerJugador);
    
    
    ActionListener actionListenerMatriz = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {  
           if(matrizFisico.puedeTrabajar) {
               if(mapa.arregloTablero[jugador.getY()][jugador.getX()] != 'V'){
                   mapa.arregloTablero[jugador.getY()][jugador.getX()]= 'X';
               }               
               matrizFisico.setArregloTablero(mapa.arregloTablero);
               matrizFisico.Pintar();
           }           
        }
    };
    
    Timer timerPintarMatriz  = new Timer(2, actionListenerMatriz);
    
    public void crearBomba() {
        limpiarBombas();
        Bomba bomb = new Bomba(mapa, jugador);               
        lstBombas.add(bomb);        
        jugador.setBombasSueltas(1);
    }
    
    public void limpiarBombas(){
        //Eliminar las que ya estan usadas previamente
        ArrayList<Integer> values = new ArrayList<>();        
        System.out.println("ArrayList Before : " + lstBombas);

        Iterator<Bomba> itr = lstBombas.iterator();
        // remove all even numbers
        while (itr.hasNext()) {
            Bomba b = itr.next();
            if (b.getEstadoBomba()>4) {
                itr.remove();
            }              
        }
        System.out.println("ArrayList After : " + lstBombas);
    }    
    
}
