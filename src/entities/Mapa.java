/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Daniel Mendez
 */
public class Mapa {
    
    //Tablero de 12x12
    // ➔ ' ' = vacio 
    // ➔ J: Posición inicial del jugador
    // ➔ X: Ladrillos del juego
    // ➔ V: Bomba   
    // ➔ B: Bonus con ladrillo
    // ➔ b: Bonus sin ladrillo
    // 
    // ➔ K: Fuego
    // ➔ L: Llave con ladrillo
    // ➔ l: Llave sin ladrillo
    
    public char[][] arregloTablero = null;
    public char[][] arregloBonus;
    public char[][] arregloLlave;
    
    public Mapa() {
        //Aca pondremos las inicializaciones
    }

    public Mapa(char[][] tablero) {
        this.arregloTablero = tablero;
    }
    
    
    
    public boolean HayLadrilloOBomba(int x, int y) {
        //No puede moverse si hay un ladrillo o una bomba (X o V)
        if(arregloTablero[y][x]!=' ' && arregloTablero[y][x]!='K' && arregloTablero[y][x]!='l' && arregloTablero[y][x]!='b'){
            return true;
        }
        return false;     
    }
    
    public void explotar(int x, int y, int range) {
        //Centro o donde nace la explosion
        arregloBonus = new char[11][11];
        arregloLlave = new char[11][11];
        arregloTablero[y][x]='K';
        int z=1;
        while((x-z)>=0&&z<range){
            if(arregloTablero[y-z][x]=='B'){
                arregloBonus[y-z][x] = 'b';
            }
            if(arregloTablero[y-z][x]=='L'){
                arregloLlave[y-z][x] = 'l';
            }
            arregloTablero[y-z][x]='K';
            z++;
        }
        z=1;
        while((x+z)<=11&&z<range){
            if(arregloTablero[y+z][x]=='B'){
                arregloBonus[y+z][x] = 'b';
            }
            if(arregloTablero[y+z][x]=='L'){
                arregloLlave[y+z][x] = 'l';
            }
            arregloTablero[y+z][x]='K';
            z++;
        }
        z=1;
        while((y-z)>=0&&z<range){
            if(arregloTablero[y][x-z]=='B'){
                arregloBonus[y][x-z] = 'b';
            }
            if(arregloTablero[y][x-z]=='L'){
                arregloLlave[y][x-z] = 'l';
            }
            arregloTablero[y][x-z]='K';
            z++;
        }
        z=1;
        while((y+z)<=11&&z<range){
            if(arregloTablero[y][x+z]=='B'){
                arregloBonus[y][x+z] = 'b';
            }
            if(arregloTablero[y][x+z]=='L'){
                arregloLlave[y][x+z] = 'l';
            }
            arregloTablero[y][x+z]='K';
            z++;
        }
    }
    
    
    public void limpiarExplosion(int x, int y, int range) {
        //Centro o donde nace la explosion
        arregloTablero[y][x]=' ';
        int z=1;
        while((x-z)>=0&&z<range){
            ponerVacioBonusLlave(x, y-z);
            z++;
        }
        z=1;
        while((x+z)<=11&&z<range){
            ponerVacioBonusLlave(x, y+z);
            z++;
        }
        z=1;
        while((y-z)>=0&&z<range){
            ponerVacioBonusLlave(x-z, y);        
            z++;
        }
        z=1;
        while((y+z)<=11&&z<range){
            ponerVacioBonusLlave(x+z, y);           
            z++;
        }
    }
    
    public void ponerVacioBonusLlave(int x, int y){
        if(arregloBonus[y][x]=='b'){
            arregloTablero[y][x] = 'b'; 
        }else if(arregloLlave[y][x]=='l'){
            arregloTablero[y][x] = 'l'; 
        }
        else{
            arregloTablero[y][x]=' ';
        }
    }
    
    
}
