/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.Timer;

/**
 *
 * @author Daniel Mendez
 */
public class Enemigo extends Elemento {

    Random generator = new Random();
    int eleccionMovimiento = 0;
    
    public Enemigo(int x, int y, Mapa mapa) {
        super(x, y, mapa);  
        timer.start();
    }
    
    ActionListener actionListenerEnemigo = new ActionListener() {        
        public void actionPerformed(ActionEvent evt) {
           boolean seMovio = false;
           while(seMovio == false){
                seMovio=false;
                eleccionMovimiento = generator.nextInt(4);
                if(eleccionMovimiento==0&&x>0&& mapa.HayLadrilloOBomba(x-1,y)==false) {
                    x--;
                    seMovio = true;
                }
                else if(eleccionMovimiento==1&&x<9&&mapa.HayLadrilloOBomba(x+1,y)==false) {
                    x++;
                    seMovio = true;
                }
                else if(eleccionMovimiento==2&&y>0&&mapa.HayLadrilloOBomba(x,y-1)==false) {
                    y--;
                    seMovio = true;
                }
                else if(eleccionMovimiento==3&&y<9&&mapa.HayLadrilloOBomba(x,y+1)==false) {
                    y++;
                    seMovio = true;
                }
                else if(mapa.HayLadrilloOBomba(x,y+1)==true && mapa.HayLadrilloOBomba(x,y-1)==true && mapa.HayLadrilloOBomba(x+1,y)==true && mapa.HayLadrilloOBomba(x-1,y)==true){
                    seMovio = true; 
                }               
           }
        }
    };
    
    Timer timer = new Timer(1000,actionListenerEnemigo);
}
