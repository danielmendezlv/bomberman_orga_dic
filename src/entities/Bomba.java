/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 *
 * @author Daniel Mendez
 */
public class Bomba {
     
    Mapa mapa;
    Jugador jugador;
    int rango,x,y;
    int estadoBomba=1;
    
    public Bomba(Mapa mapa, Jugador jugador) {
        this.mapa = mapa;
        this.jugador = jugador;     
        x=jugador.getX();
        y=jugador.getY();
        rango = jugador.getRangoBomba();
        timer.start();        
        mapa.arregloTablero[y][x]='V';
    }

    public int getEstadoBomba() {
        return estadoBomba;
    }

    public void setEstadoBomba(int estadoBomba) {
        this.estadoBomba = estadoBomba;
    }
    
    
    ActionListener actionListenerBomba = new ActionListener() {
        //Estado 1 = Se coloco la bomba
        //Estado 2 = Explotar
        //Estado 3 = Dejar explotando
        //Estado 4 = Desvanecer
        public void actionPerformed(ActionEvent evt) {
            if(estadoBomba==2){
                mapa.explotar(x,y,rango);
            }            
            else if(estadoBomba==4) {
               mapa.limpiarExplosion(x,y,rango);
               jugador.setBombasSueltas(-1);
               timer.stop();
            }
            estadoBomba++;
        }
    };

    Timer timer = new Timer(1000, actionListenerBomba);

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }
    
    
}
