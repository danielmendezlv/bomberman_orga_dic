/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Daniel Mendez
 */
public class Jugador extends Elemento{
    
    private int rangoBomba, bombasDisponibles , bombasSueltas, vidas;
    
    public Jugador(int x, int y, Mapa mapa) {
        super(x, y , mapa);
        //Aca deberia de ser 1 pero como el Z o el While lo inizo con 1, esta bien (Ver que el Z es dependiente 
        //Las funciones de explosion y restablecer
        rangoBomba = 2;
        bombasDisponibles = 1;
        bombasSueltas = 0;
        vidas=3;
    }

    public int getVidas() {
        return vidas;
    }

    public void setVidas(int x) {
        this.vidas += x;
    }

    
    public int getRangoBomba() {
        return rangoBomba;
    }

    public void setRangoBomba(int x) {
        this.rangoBomba+=x;
    }

    public int getBombasDisponibles() {
        return bombasDisponibles;
    }

    public void setBombasDisponibles(int x) {
        this.bombasDisponibles += x;
    }

    public int getBombasSueltas() {
        return bombasSueltas;
    }

    public void setBombasSueltas(int x) {
        this.bombasSueltas += x;
    }

    public void arriba() {
        if(y>0&&mapa.HayLadrilloOBomba(x,y-1)==false) {
            modRango(x, y-1);
            rellenar();
            y-=1;
        }
    }

    public void abajo() {
        if(y<11&&mapa.HayLadrilloOBomba(x,y+1)==false) {
            modRango(x, y+1);
            rellenar();
            y+=1;
        }
    }

    public void izquierda() {
        if(x>0&&mapa.HayLadrilloOBomba(x-1,y)==false) {
            modRango(x-1, y);
            rellenar();
            x-=1;
        }
    }
    
    public void derecha() {
        if(x<12&&mapa.HayLadrilloOBomba(x+1,y)==false) {
            modRango(x+1, y);
            rellenar();
            x+=1;
        }
    }
    
    public void modRango(int x, int y){
        if(mapa.arregloTablero[y][x]=='b'){
            System.out.println(this.rangoBomba);
            setRangoBomba(1);
        }
    }
    
    public void rellenar(){
        //Si no se acaba de poner una bomba
        if(mapa.arregloTablero[y][x]!='V'){
            //Rellenar el espacio con vacio
            mapa.arregloTablero[y][x]=' ';
        } 
    }
            
    
}
